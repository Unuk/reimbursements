#include <Db.h>

Db::Db(const QString &path) : m_conf_path(path), m_db(new QSqlDatabase()) { }

Db::~Db()
{
  if(m_db->isOpen())
    m_db->close();
  //QSqlDatabase::removeDatabase("QMYSQL");
}

void Db::Open()
{
  if(!QFile(m_conf_path).exists())
    throw std::runtime_error("Configure file doesn't exists");

  QSettings conf(m_conf_path, QSettings::IniFormat);
  if(!conf.contains("hostname") || !conf.contains("username") || !conf.contains("password") || !conf.contains("database") || !conf.contains("table_prices"))
    throw std::runtime_error("Configure file is broken");

  m_db = make_shared<QSqlDatabase>(QSqlDatabase::addDatabase("QMYSQL"));
  m_db->setHostName(conf.value("hostname").toString());
  m_db->setDatabaseName(conf.value("database").toString());
  m_db->setUserName(conf.value("username").toString());
  m_db->setPassword(conf.value("password").toString());

  if(!m_db->open())
    throw std::runtime_error("Can't open database connection. " + m_db->lastError().text().toStdString());

  m_query = make_shared<QSqlQuery>();
  m_table_prices = conf.value("table_prices").toString().toStdString();
}

void Db::getItems(list<Db::Item>* items)
{
  if(!Execute("SELECT id, name, type FROM " + m_table_prices))
    throw std::runtime_error("Can't select prices. " + m_db->lastError().text().toStdString());

  while(m_query->next())
  {
    Item row;
    row.id = m_query->value("id").toInt();
    row.name = m_query->value("name").toString().toStdString();
    row.type = m_query->value("type").toInt();
    items->push_back(row);
  }
}

void Db::updatePrice(Item item)
{
    string query_insurance = (item.type == 2 || item.type == 3 || item.type == 4) ? ", insurance_cost='"
        + to_string(item.insurance_cost) + "', insurance_payout='" +  to_string(item.insurance_payout)
        + "'" : "";
    string query = "UPDATE " + m_table_prices + " SET name='" + item.name + "', Price='" + to_string(item.price) + "'"
        + query_insurance + " WHERE id='" + to_string(item.id) + "'";
    if(!Execute(query))
      throw std::runtime_error("Can't update price. " + m_db->lastError().text().toStdString());
}

bool Db::Execute(string query)
{
  if(!m_db->isOpen()) return false;
  return m_query->exec(QString::fromStdString(query));
}
