#include <Price.h>

#include <QtCore>
#include <QDir>

int main(int argc, char *argv[])
{
  QCoreApplication a(argc, argv);
  QString path;
  if(argc == 2)
    path = argv[1];
  else
    path = QDir::currentPath().append("/etc/reimbursements.conf");
  Price p(path);
  p.updatePrice();

  //return a.exec();
}
