#include <Price.h>

Price::Price(const QString &path) : m_db(new Db(path)), m_crest(new Crest()) { }

Price::~Price() { }

void Price::updatePrice()
{
  log(QString("Reimbursements update start: ").append(QDateTime::currentDateTime().toString("dd.MM.yyyy HH:mm:ss")).toStdString());
  try
  {
    m_db->Open();
    log("Load items");
    m_db->getItems(&m_items);
    vector<int> insurance_id;
    log("Fill insurances");
    getItemsList(&insurance_id);
    fillInsurances(insurance_id.data(), insurance_id.size());
    log("Update items:");
    fillItems();
  }
  catch(std::exception& e)
  {
    log(e.what());
  }
  log(QString("Reimbursements update end: ").append(QDateTime::currentDateTime().toString("dd.MM.yyyy HH:mm:ss")).toStdString());
}

void Price::getItemsList(vector<int>* ids)
{
  for(Db::Item item : m_items)
  {
    if(item.type == 2 || item.type == 3 || item.type == 4) ids->push_back(item.id);
  }
}

void Price::fillItems()
{
  unsigned int index = 0;
  const unsigned int size = m_items.size();
  for(Db::Item &item : m_items)
  {
    index++;
    if (item.name == "") item.name = m_crest->getName(item.id);
    log("[" + to_string(index) + "/" + to_string(size) + "]\tUpdate " + item.name);
    /* if capital -> search in region
     * 10000002 - The Forge
     * 60003760 - Jita IV - Moon 4 - Caldari Navy Assembly Plant */
    item.price = (item.type == 3) ? m_crest->getMarketPrice(item.id, 10000002 , "sell", 0) : m_crest->getMarketPrice(item.id, 10000002 , "sell", 60003760);
    m_db->updatePrice(item);
  }
}

void Price::fillInsurances(int *ids, int size)
{
  list<Crest::Insurance> insurances = m_crest->getInsurance(ids, size, "Platinum");
  for(Db::Item &item : m_items)
  {
    foreach(Crest::Insurance insurance, insurances)
    {
      if(insurance.id == item.id)
      {
        item.insurance_cost = insurance.cost;
        item.insurance_payout = insurance.payout;
      }
    }
  }
}

void Price::log(string text)
{
  cout << text << endl;
}
