CREATE TABLE `reimbursements_prices` (
  `id` int(11) NOT NULL DEFAULT '0',
  `name` tinytext,
  `type` int(2) DEFAULT NULL,
  `price` int(32) unsigned DEFAULT NULL,
  `insurance_cost` int(32) unsigned DEFAULT NULL,
  `insurance_payout` int(32) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8
