#include <Db.h>
#include <Crest.h>

#include <QDateTime>

#include <memory>
#include <stdexcept>
#include <iostream>

using namespace std;

class Price
{
public:
  Price(const QString &path);
  ~Price();

  void updatePrice();

private:

  void fillItems();
  void fillInsurances(int* ids, int size);
  void getItemsList(vector<int>* ids);
  void log(string text);

  shared_ptr<Db> m_db;
  shared_ptr<Crest> m_crest;
  list<Db::Item> m_items;
};
