#ifndef IAPETUS_CREST_H
#define IAPETUS_CREST_H

#include <QString>
#include <QUrl>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonValue>
#include <QJsonArray>
#include <QJsonObject>
#include <QEventLoop>

#include <list>
#include <stdexcept>
#include <memory>

using namespace std;

class Crest
{
public:
  struct Insurance
  {
    Insurance() : id(0), cost(0), payout(0) { }
    int id;
    unsigned int cost;
    unsigned int payout;
  };

  Crest(string url = "https://crest-tq.eveonline.com/");
  ~Crest();
  string getName(int id);
  unsigned int getMarketPrice(int itemID, int regionID, string orderType, int stationID = 0);
  list<Crest::Insurance> getInsurance(int* ids, int size, string type);

private:
  QString getJson(QString param);

  unique_ptr<QNetworkAccessManager> m_namanager;
  QString m_url;
  QJsonParseError m_err;
};

#endif //IAPETUS_CREST_H
