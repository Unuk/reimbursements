#include <QSettings>
#include <QFile>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>

#include <memory>
#include <stdexcept>

using namespace std;

class Db
{
public:
  struct Item
  {
    Item() : id(0), price(0), insurance_cost(0), insurance_payout(0) { }
    int id;
    int type;
    unsigned int price;
    unsigned int insurance_cost;
    unsigned int insurance_payout;
    string name;
    /* type = 0, 1 - module
     * type = 2 - subcapital
     * type = 3 - capital
     * type = 4 - ships with partial compensation
     */
  };

  Db(const QString &path);
  ~Db();
  void Open();
  void getItems(list<Item>* items);
  void updatePrice(Item item);

private:
  bool Execute(string query);

  const QString m_conf_path;
  string m_table_prices;
  shared_ptr<QSqlDatabase> m_db;
  shared_ptr<QSqlQuery> m_query;
};
