#include "Db.h"

#include <QCoreApplication>

#include <gtest/gtest.h>

class DbTest : public ::testing::Test
{
public:
  Db * db;

  virtual void SetUp()
  {
    db = new Db("etc/reimbursements.conf");
  }

  virtual void TearDown()
  {
    delete db;
  }
};

TEST_F(DbTest, checkConnection)
{
  ASSERT_NO_THROW(db->Open());
}

TEST_F(DbTest, getItems)
{
  db->Open();
  list<Db::Item> items;
  ASSERT_NO_THROW(db->getItems(&items));
}

int main(int argc, char** argv)
{
  QCoreApplication a(argc, argv);
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
